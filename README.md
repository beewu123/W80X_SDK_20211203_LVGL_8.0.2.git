# W80X_SDK_20211203_LVGL_8.0.2

#### 介绍
```
SDK：wm_sdk_w800_20211203
LVGL：8.0.2
```

#### 使用说明

配置SPI GPIO以及屏幕尺寸
```
--->app/lcd.h

#define LCD_SCL        	WM_IO_PB_15 //--->>TFT --SCL
#define LCD_SDA        	WM_IO_PB_17 //--->>TFT --SDA
#define LCD_RST     	WM_IO_PB_24	//--->>TFT --RST
#define LCD_DC         	WM_IO_PB_25	//--->>TFT --RS/DC
#define LCD_CS        	WM_IO_PB_14 //--->>TFT --CS
#define LCD_BL        	WM_IO_PB_27 //--->>TFT --BL

#define X_MAX_PIXEL	        (128)
#define Y_MAX_PIXEL	        (160)

--->lvgl/lv_port_disp.c     

disp_drv.hor_res = 128;
disp_drv.ver_res = 160;
```

#### 参与贡献
```
https://www.winnermicro.com/html/1/156/158/558.html
https://lvgl.io/
```


