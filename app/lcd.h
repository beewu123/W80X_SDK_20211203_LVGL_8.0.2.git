#ifndef _LCD_H_
#define _LCD_H_
#include "lcd.h"
#include "wm_gpio_afsel.h"

#define LCD_SCL        	WM_IO_PB_15 //--->>TFT --SCL
#define LCD_SDA        	WM_IO_PB_17 //--->>TFT --SDA
#define LCD_RST     	WM_IO_PB_24	//--->>TFT --RST
#define LCD_DC         	WM_IO_PB_25	//--->>TFT --RS/DC
#define LCD_CS        	WM_IO_PB_14 //--->>TFT --CS
#define LCD_BL        	WM_IO_PB_27 //--->>TFT --BL

#define LCD_DC_SET   do{ tls_gpio_write(LCD_DC, 1);} while(0)   
#define LCD_LED_SET  do{ tls_gpio_write(LCD_BL, 1);} while(0)   
#define LCD_RST_SET  do{ tls_gpio_write(LCD_RST,1);} while(0)  

#define LCD_DC_CLR   do{ tls_gpio_write(LCD_DC, 0);} while(0) 
#define LCD_LED_CLR  do{ tls_gpio_write(LCD_BL, 0);} while(0)   
#define LCD_RST_CLR  do{ tls_gpio_write(LCD_RST,0);} while(0)     


#define X_MAX_PIXEL	        (128)
#define Y_MAX_PIXEL	        (160)


#define RED  	0xF800
#define GREEN	0x07E0
#define BLUE 	0x001F
#define WHITE	0xFFFF
#define BLACK	0x0000
#define YELLOW  0xFFE0
#define GRAY0   0xEF7D   	    
#define GRAY1   0x8410      	
#define GRAY2   0x4208      	


void LCD_GpioInit(void);

void LCD_Reset(void);

void LCD_Back_On(void);

void LCD_Back_Off(void);

void LCD_Init(void);


void LCD_Address_Set(u16 x_start, u16 y_start, u16 x_end, u16 y_end);

void LCD_Fill(u16 x_start, u16 y_start, u16 x_end, u16 y_end, u16 color);

void LCD_Clear(u16 Color);

void LCD_DrawPoint(u16 x, u16 y, u16 color);

void LCD_DrawLine(u16 x_start, u16 y_start, u16 x_end, u16 y_end, u16 color);

void LCD_DrawRectangle(u16 x_start, u16 y_start, u16 x_end, u16 y_end, u16 color);

void LCD_DrawCircle(u16 x, u16 y, u8 r, u16 color);

void LCD_ShowPicture(u16 x, u16 y, u16 width, u16 height, u8 *data);


#endif