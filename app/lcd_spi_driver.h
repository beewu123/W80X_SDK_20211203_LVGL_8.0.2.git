
#ifndef __LCD_SPI_DRIVER_H__
#define __LCD_SPI_DRIVER_H__
#include "wm_include.h"

void S_WriteReg(u8 reg);

void S_WriteData8(u8 data);

void S_WriteData16(u16 data);

void S_WriteData(u8 *data, u32 len);

#endif
