/***************************************************************************** 
* 
* File Name : main.c
* 
* Description: main 
* 
* Copyright (c) 2014 Winner Micro Electronic Design Co., Ltd. 
* All rights reserved. 
* 
* Author : dave
* 
* Date : 2014-6-14
*****************************************************************************/ 
#include "wm_include.h"
#include "wm_cpu.h"
#include "lcd.h"
#include "picture.h"
#include "lvgl.h"
#include "wm_timer.h"



//lv_ui guider_ui;
lv_obj_t *screen = NULL;
lv_disp_drv_t *_disp_drv = NULL;

void LCD_Flush(lv_disp_drv_t * disp_drv, uint16_t x, uint16_t y, uint16_t length, uint16_t width, uint8_t *data)
{
    _disp_drv = disp_drv;
    LCD_ShowPicture(x, y, length, width, data);
    if (_disp_drv) {
        lv_disp_flush_ready(_disp_drv);
    }	
}

static void tick_timer_irq(u8 *arg)
{
	lv_tick_inc(1);
	// printf("timer irq\n");
}


int create_tick_timer(void)
{

	u8 timer_id;
	struct tls_timer_cfg timer_cfg;
	
	timer_cfg.unit = TLS_TIMER_UNIT_MS;
	timer_cfg.timeout = 1;
	timer_cfg.is_repeat = 1;
	timer_cfg.callback = (tls_timer_irq_callback)tick_timer_irq;
	timer_cfg.arg = NULL;
	timer_id = tls_timer_create(&timer_cfg);
	tls_timer_start(timer_id);
	// printf("timer start\n");	

	return WM_SUCCESS;
}
void UserMain(void)
{
	// extern void tls_sys_clk_set();
	tls_sys_clk_set(CPU_CLK_80M);
	extern int tls_get_mac_addr(u8 *mac);
	extern int tls_get_bt_mac_addr(u8 *mac);
	extern u32 tls_mem_get_avail_heapsize(void);
	u8 mac[6]={0};
	u8 btmac[6]={0};
	tls_get_mac_addr(mac);
	tls_get_bt_mac_addr(btmac);
	printf("             \\\\\\|///\n");
	printf("           \\\\  .-.-  //\n");
	printf(".           (  .@.@  )  \n");
	printf("+-------oOOo-----(_)-----oOOo---------+\n\n");
	printf(" ---> Compile "__DATE__", "__TIME__"\n");
	printf(" ---> %s.c\r\n",__func__);
	printf(" ---> GetHeap:%d\n",tls_mem_get_avail_heapsize());
	printf(" ---> BLE  MAC:%02X%02X%02X%02X%02X%02X\n",btmac[0],btmac[1],btmac[2],btmac[3],btmac[4],btmac[5]);
	printf(" ---> WIFI MAC:%02X%02X%02X%02X%02X%02X\n",mac[0],mac[1],mac[2],mac[3],mac[4],mac[5]);
	printf("\n+---------------------Oooo------------+\n");

	printf("\n user task \n");
	


	LCD_Init();

	create_tick_timer();

	lv_init();
    lv_port_disp_init();

	screen = lv_obj_create(NULL);
	lv_scr_load(screen);

    lv_obj_t *btn = lv_btn_create(lv_scr_act());
    lv_obj_set_pos(btn, 0, 60);
    lv_obj_set_size(btn, 120, 50);
    lv_obj_t *label = lv_label_create(btn);
    lv_label_set_text(label, "Button");

    lv_obj_t *checkbox = lv_checkbox_create(lv_scr_act());
    lv_obj_set_pos(checkbox, 0, 120);
    lv_obj_set_size(checkbox, 120, 50);
    lv_checkbox_set_text(checkbox, "Are you ready");

	while (1)
	{
		tls_os_time_delay(5);
		lv_task_handler();
	}

	// LCD_Clear(WHITE);
	// while (1)
	// {
	// 	LCD_ShowPicture(0, 0, 128, 160, (unsigned char*)gImage_gif);
	// 	tls_os_time_delay(500);
	// 	LCD_ShowPicture(0, 0, 128, 160, (unsigned char*)gImage_tu);
	// 	tls_os_time_delay(500);
	// }



#if DEMO_CONSOLE
	CreateDemoTask();
#endif
//用户自己的task
}
