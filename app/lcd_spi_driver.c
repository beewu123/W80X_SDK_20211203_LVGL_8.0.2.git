#include "wm_include.h"
#include "lcd_spi_driver.h"
#include "wm_hostspi.h"
#include "lcd.h"

void S_WriteReg(u8 reg)
{
	LCD_DC_CLR;
	tls_spi_write(&reg, 1);
	LCD_DC_SET;
}

void S_WriteData8(u8 data)
{
	tls_spi_write(&data, 1);
}

void S_WriteData16(u16 data)
{
	u8 temp[2];
	
	temp[0] = data >> 8;
	temp[1] = data;
	tls_spi_write(temp, 2);
}


static void SPI_Transmit_dma(u8 *pData, u32 Size)
{
	u32 body = Size/8160;
	u32 tail = Size%8160;
	u32 offset = 0;

	LCD_DC_SET;

	if(body > 0)
	{
		do
		{
			tls_spi_write((u8 *)pData+offset*8160, 8160);
			offset++;
		} while (--body);
	}
	tls_spi_write((u8 *)pData+(offset*8160), tail);
}

void S_WriteData(u8 *data, u32 len)
{
	SPI_Transmit_dma(data, len);
}
